/* ----- [ ROLES ] ----- */

CREATE TABLE IF NOT EXISTS roles (
  id INTEGER,
  name TEXT NOT NULL,
  desc TEXT DEFAULT 'NO DESCRIPTION',
  CONSTRAINT pk_roles PRIMARY KEY (id),
  CONSTRAINT uk_roles_name UNIQUE (name)
);

CREATE UNIQUE INDEX IF NOT EXISTS ux_roles_name ON roles (name);

/* ----- [ USERS ] ----- */

CREATE TABLE IF NOT EXISTS users (
    id INTEGER,
    username TEXT NOT NULL,
    password TEXT NOT NULL,
    role INTEGER NOT NULL DEFAULT 2,  -- Default user
    CONSTRAINT pk_users PRIMARY KEY (id),
    CONSTRAINT uk_users_username UNIQUE (username),
    CONSTRAINT fk_users_roles FOREIGN KEY (role) REFERENCES roles (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS ux_users_username ON users (username);
CREATE INDEX IF NOT EXISTS ix_users_role ON users (role);

/* ----- [ TITLES ] ----- */

CREATE TABLE IF NOT EXISTS titles (
    id INTEGER,
    name TEXT NOT NULL,
    desc TEXT DEFAULT 'NO DESCRIPTION',
    CONSTRAINT pk_titles PRIMARY KEY (id),
    CONSTRAINT uk_titles_name UNIQUE (name)
);

CREATE UNIQUE INDEX IF NOT EXISTS ux_titles_name ON titles (name);

/* ----- [ USERS - TITLES ] ----- */

CREATE TABLE IF NOT EXISTS users_titles (
    users_id INTEGER,
    titles_id INTEGER,
    CONSTRAINT fk_users_titles_users FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fk_users_titles_titles FOREIGN KEY (titles_id) REFERENCES titles (id) ON DELETE CASCADE ON UPDATE CASCADE
);

ANALYZE;