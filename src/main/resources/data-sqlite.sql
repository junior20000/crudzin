/* ----- [ ROLES INITIALIZATION ] ----- */

INSERT INTO roles(id, name, desc)
SELECT 1, 'ADMIN', 'Ultimate Admin'
WHERE NOT EXISTS(SELECT 1 FROM roles WHERE ROWID=1);

INSERT INTO roles(id, name, desc)
SELECT 2, 'USER', 'Just an ordinary user'
WHERE NOT EXISTS(SELECT 1 FROM roles WHERE ROWID=2);

/* ----- [ USERS INITIALIZATION ] ----- */


INSERT INTO users(id, username, password, role)
SELECT 1, 'Orichalcos', '$argon2i$v=19$m=48,t=20,p=2$NnVtUFROaWp2WHp6UXdBMQ$l6RrAAjopp+G9WvP9byE/Vn8QIPcffYSnQXbtf5VBHo/TnEevkJm7F5fDoAMuH09QZJh5vyp/CYx4L4MUGMXLE3Rrrnih+chH7PBTXGmiZUBOB1qreLy46g0IC5ErbAj7UgTwA', 1  /* PASSWORD: 12345678 */
WHERE NOT EXISTS(SELECT 1 FROM users WHERE ROWID=1);

INSERT INTO users(id, username, password, role)
SELECT 2, 'OrichalcosKyotora', '$argon2i$v=19$m=48,t=20,p=2$VTJwY015ZGFvNEQyY2VMdA$fHWi1rAIFppSDiUrZKqSHHNubTzlnIpIdT6ib5mTOWFQuhJcFAoLavxzUTwfBIPpeYO8EnuHWgWZoh35DVhjGG/qNpFdIGolYCnxP4t30MhuUWYjwHDDQXV7nH6n/Ytdh7XF9A', 1  /* PASSWORD: 123456789*/
WHERE NOT EXISTS(SELECT 1 FROM users WHERE ROWID=2);

INSERT INTO users(id, username, password, role)
SELECT 3, 'Metamorphic', '$argon2i$v=19$m=48,t=20,p=2$TWtNSjB2TmpmMXkwMzZ5eQ$+8fvKaI1pn3zvDTI5pyC1oFtEX4As2nzLXmFHy4IruNIaQYgrMUkAGGX1sVeL+mUY0/Y4Q5Ld5MBirfAY/casb9OtmlrB40j+dDTpMGNgMYEHYzX4Dx3omRAsq4iitYspl0y0g', 2  /* PASSWORD: cLKb75pw85vmBWkqscZtTnQmfNdLK8Mj */
WHERE NOT EXISTS(SELECT 1 FROM users WHERE ROWID=3);

INSERT INTO users(id, username, password, role)
SELECT 4, 'DefaultUser', '$argon2i$v=19$m=48,t=20,p=2$cEplRm9VcnEzRzRVY1BiaQ$Lg4gGtJ33Kr63TVcRIW09Kbm4m8TsAwr3Q96zcyMO/Y6NKRA7Gc1ZMFJBk7h8MK4lLBqaLRMpRwvR1dMsjg0sxvrzOkSiur7Pis1hKKuPmXh2qZh9cjAI1R7R+8mLCmsHL0YDg', 2  /* PASSWORD: t9Ste@S_(W=u?cu:=z */
WHERE NOT EXISTS(SELECT 1 FROM users WHERE ROWID=4);