<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%--
  ~ Copyright (c) 2020, Roberto Schiavelli Júnior - All Rights Reserved
  ~ Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
  ~ Proprietary and confidential
  ~ Written by Roberto Schiavelli Júnior
  ~ Last time modified: 14/06/2020 16:06.
  --%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Online Users</title>
    <link rel="stylesheet" href="<c:url value="/css/main_resp.css"/>">
</head>
<body>
<div class="panel">
    <jsp:include page="./tiles/header.jsp"/>
    <main class="panel__main">
        <h1 class="panel__title">Online users</h1>
        <div class="online_users__wrapper">
            <c:if test="${not empty onlineUsers}">
                <c:forEach items="${onlineUsers}" var="user">
                    <div class="info_block info_block--mini">
                        <div class="info_block__row">
                            <div class="info_block__header">Username</div>
                            <div class="info_block__data">${user.username}</div>
                        </div>
                        <div class="info_block__row">
                            <div class="info_block__header">Role</div>
                            <div class="info_block__data">${user.role.name}</div>
                        </div>
                    </div>
                </c:forEach>
            </c:if>
        </div>
    </main>
    <jsp:include page="./tiles/footer.jsp"/>
</div>
</body>
</html>