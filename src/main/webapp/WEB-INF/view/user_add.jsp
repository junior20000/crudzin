<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--
  ~ Copyright (c) 2020, Roberto Schiavelli Júnior - All Rights Reserved
  ~ Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
  ~ Proprietary and confidential
  ~ Written by Roberto Schiavelli Júnior
  ~ Last time modified: 14/06/2020 16:06.
  --%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add User</title>
    <link rel="stylesheet" href="<c:url value="/css/main_resp.css"/>">
</head>
<body>
<div class="panel">
    <jsp:include page="./tiles/header.jsp"/>
    <main class="panel__main">
        <h1 class="panel__title">Add New User</h1>
        <form:form action="/admin/add/user" method="post" modelAttribute="userForm" cssClass="form">
            <security:csrfInput/>
            <div class="form__row">
                <s:bind path="username">
                    <form:label path="username">Username
                        <form:input type="text" class="input input--rounded" path="username" autocomplete="off" autofocus="autofocus" tabindex="1"/>
                    </form:label>
                    <div class="form__alert">
                        <c:forEach items="${status.errorMessages}" var="error">
                            <div class="alert alert--danger">${error}</div>
                        </c:forEach>
                    </div>
                </s:bind>
            </div>
            <div class="form__row">
                <s:bind path="password">
                    <form:label path="password">Password
                        <form:input type="password" class="input input--rounded" path="password" autocomplete="off" autofocus="autofocus" tabindex="2"/>
                    </form:label>
                    <div class="form__alert">
                        <c:forEach items="${status.errorMessages}" var="error">
                            <div class="alert alert--danger">${error}</div>
                        </c:forEach>
                    </div>
                </s:bind>
            </div>
            <div class="form__row">
                <s:bind path="confirmPassword">
                    <form:label path="confirmPassword">Confirm password
                        <form:input type="password" class="input input--rounded" path="confirmPassword" autocomplete="off" autofocus="autofocus" tabindex="3"/>
                    </form:label>
                    <div class="form__alert">
                        <c:forEach items="${status.errorMessages}" var="error">
                            <div class="alert alert--danger">${error}</div>
                        </c:forEach>
                    </div>
                </s:bind>
            </div>
            <div class="form__row">
                <form:label path="role">Role
                    <form:select path="role" items="${role_list}" itemLabel="name" itemValue="id" class="select select--rounded" tabindex="4"/>
                </form:label>
            </div>
            <div class="form__row form__row--large form__row--center">
                <form:button type="submit" class="button button--large" tabindex="5">Confirm</form:button>
            </div>
        </form:form>
    </main>
    <jsp:include page="./tiles/footer.jsp" flush="true"/>
</div>
</body>
</html>
