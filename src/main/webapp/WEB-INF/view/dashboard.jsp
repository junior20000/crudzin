<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  ~ Copyright (c) 2020, Roberto Schiavelli Júnior - All Rights Reserved
  ~ Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
  ~ Proprietary and confidential
  ~ Written by Roberto Schiavelli Júnior
  ~ Last time modified: 14/06/2020 16:06.
  --%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <%@ page isELIgnored="false"%>
    <link rel="stylesheet" href="<c:url value="/css/main_resp.css"/>">
</head>
<body>
<div class="panel">
    <jsp:include page="./tiles/header.jsp" flush="true"/>
    <main class="panel__main">
        <h1 class="panel__title">Dashboard</h1>
        <%--@elvariable id="alert_success" type="String"--%>
        <c:if test="${not empty alert_success}">
            <div class="dashboard--alert">
                <div class="alert alert--success">${alert_success}</div>
            </div>
        </c:if>
        <div class="panel__button-center">
            <a href="<spring:eval expression="@environment.getProperty('admin.add.user.urn')"/>" class="button button--large">Add User</a>
        </div>
        <table class="table table--large">
            <thead class="table__head">
            <tr class="table__row">
                <c:forTokens items="ID,Username,Password,Role,Action" delims="," var="headers">
                    <c:choose>
                        <c:when test="${(headers == 'ID') or (headers == 'Role') or (headers == 'Action')}">
                            <th class="table--center">${headers}</th>
                        </c:when>
                        <c:otherwise>
                            <th>${headers}</th>
                        </c:otherwise>
                    </c:choose>
                </c:forTokens>
            </tr>
            </thead>
            <tbody class="table__body">
            <c:forEach items="${listUsers}" var="user">
                <tr class="table__row">
                    <td class="table--center" data-label="ID">${user.id}</td>
                    <td data-label="Username">${user.username}</td>
                    <td data-label="Password">${user.password}</td>
                    <td class="table--center" data-label="Role">${user.role.name}</td>
                    <td class="table--center" data-label="Action">
                        <div class="table__action-container-responsive">
                            <a href="<spring:eval expression="@environment.getProperty('admin.edit.user.urn')"/>/${user.id}" class="button button--table">EDIT</a>
                            <a href="<spring:eval expression="@environment.getProperty('admin.delete.user.urn')"/>/${user.id}" class="button button--table">Delete</a>
                        </div>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </main>
    <jsp:include page="./tiles/footer.jsp" flush="true"/>
</div>
</body>
</html>