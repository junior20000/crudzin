<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%--
  ~ Copyright (c) 2020, Roberto Schiavelli Júnior - All Rights Reserved
  ~ Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
  ~ Proprietary and confidential
  ~ Written by Roberto Schiavelli Júnior
  ~ Last time modified: 14/06/2020 16:06.
  --%>

<header class="header">
    <span class="header__logo">CRUDzin</span>
    <nav class="nav">
        <ul>
            <security:authorize access="isAnonymous()">
                <li class="nav__link-container"><a href="#" class="nav__link">Home</a> </li>
                <li class="nav__link-container"><a href="#" class="nav__link">Support</a> </li>
                <li class="nav__link-container"><a href="#" class="nav__link">FAQ</a> </li>
                <li class="nav__link-container"><a href="#" class="nav__link">About Us</a> </li>
            </security:authorize>
            <security:authorize access="isAuthenticated()">
                <security:authorize access="hasRole('ROLE_ADMIN')">
                    <li class="nav__link-container"><a href="/admin/dashboard" class="nav__link${requestScope['javax.servlet.forward.request_uri'] == "/admin/dashboard" || requestScope['javax.servlet.forward.request_uri'] == "/admin" ? " nav__link--active" : ""}" tabindex="6">Dashboard</a></li>
                </security:authorize>
                <li class="nav__link-container"><a href="#" class="nav__link" tabindex="7">Profile</a></li>
            </security:authorize>
        </ul>
    </nav>
    <security:authorize access="isAnonymous()">
        <a href="#" class="button button--logout" tabindex="8">Login</a>
    </security:authorize>
    <security:authorize access="isAuthenticated()">
        <form:form action="/logout" method="post">
            <security:csrfInput/>
            <input type="submit" value="Logout" class="button button--logout" tabindex="8"/>
        </form:form>
    </security:authorize>
</header>