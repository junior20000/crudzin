<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%--
  ~ Copyright (c) 2020, Roberto Schiavelli Júnior - All Rights Reserved
  ~ Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
  ~ Proprietary and confidential
  ~ Written by Roberto Schiavelli Júnior
  ~ Last time modified: 14/06/2020 16:06.
  --%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link rel="stylesheet" href="<c:url value="/css/main_resp.css"/>">
</head>
<body>
<div class="panel">
    <jsp:include page="./tiles/header.jsp" flush="true"/>
    <main class="panel__main">
        <h1 class="panel__title">Delete User</h1>
        <div class="info_block">
            <div class="info_block__row">
                <div class="info_block__header">ID</div>
                <div class="info_block__data">${user.id}</div>
            </div>
            <div class="info_block__row">
                <div class="info_block__header">Username</div>
                <div class="info_block__data">${user.username}</div>
            </div>
            <div class="info_block__row">
                <div class="info_block__header">Password</div>
                <div class="info_block__data">${user.password}</div>
            </div>
            <div class="info_block__row">
                <div class="info_block__header">Role</div>
                <div class="info_block__data">${user.role.name}</div>
            </div>
        </div>
        <form action="<spring:eval expression="@environment.getProperty('admin.delete.user.urn')"/>/${user.id}" method="POST" class="panel__center">
            <security:csrfInput/>
            <input type="submit" value="Delete" class="button button--small-rounded-50">
        </form>
    </main>
    <jsp:include page="./tiles/footer.jsp" flush="true"/>
</div>
</body>
</html>