<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%--
  ~ Copyright (c) 2020, Roberto Schiavelli Júnior - All Rights Reserved
  ~ Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
  ~ Proprietary and confidential
  ~ Written by Roberto Schiavelli Júnior
  ~ Last time modified: 14/06/2020 16:04.
  --%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Atenção família</title>
    <link rel="stylesheet" href="<c:url value="/css/main_resp.css"/>">
</head>
<body>
<video class="eae eae--full" autoplay="autoplay" loop="loop">
    <source src="<c:url value="./media/video/Grupo_hackeado.mp4"/>" type="video/mp4">
</video>
</body>
</html>