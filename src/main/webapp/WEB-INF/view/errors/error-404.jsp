<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html;charset=UTF-8" %>
<%--
  ~ Copyright (c) 2020, Roberto Schiavelli Júnior - All Rights Reserved
  ~ Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
  ~ Proprietary and confidential
  ~ Written by Roberto Schiavelli Júnior
  ~ Last time modified: 14/06/2020 16:04.
  --%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>404 - Page not found</title>
    <link rel="stylesheet" href="<c:url value="/css/main_resp.css"/>">
</head>
<body>
<div class="panel">
    <jsp:include page="../tiles/header.jsp" flush="true"/>
    <main class="panel__main">
        <h1 class="panel__title">Page not found</h1>
        <h2 class="panel__subtitle">Error 404</h2>
        <div class="error-403__doge-wrapper">
            <div class="panel__img-center" id="error-404__doge-img"></div>
            <a href="<spring:eval expression="@environment.getProperty('admin.root')"/>" class="button button--small-rounded-50" id="error-404__redir-button" tabindex="1">It's OK, I will just return</a>
            <h3 class="panel__nano-title" id="error-404__doge-talk">Somry, I couldm't much fimd the page</h3>
        </div>
    </main>
    <jsp:include page="../tiles/footer.jsp" flush="true"/>
</div>
</body>
</html>