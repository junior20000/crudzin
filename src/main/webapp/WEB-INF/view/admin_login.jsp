<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%--
  ~ Copyright (c) 2020, Roberto Schiavelli Júnior - All Rights Reserved
  ~ Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
  ~ Proprietary and confidential
  ~ Written by Roberto Schiavelli Júnior
  ~ Last time modified: 14/06/2020 16:06.
  --%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Login</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/main_resp.css"/>">
</head>
<body>
<div class="panel">
    <jsp:include page="./tiles/header.jsp"/>
    <main class="panel__main">
        <section class="main">
            <div class="CRUDzin-Slogan">
                <h1>
                    <strong class="CRUD">CRUD<b class="zin">zin</b></strong>
                </h1>
                <p class="slogan">
                    <b>CREATE, READ, UPDATE, DELETE N'JOY</b>
                </p>
            </div>
            <spring:eval expression="@environment.getProperty('admin.root')" var="formAction"/>
            <form:form action="${formAction}" method="POST" modelAttribute="userForm" cssClass="form">
                <security:csrfInput/>
                <div class="form__row">
                    <form:label path="username">Admin</form:label>
                    <form:input type="text" class="input input--rounded" path="username" autocomplete="off" autofocus="autofocus" value="${not empty username ? username : ''}" tabindex="1"/>
                </div>
                <div class="form__row">
                    <form:label path="password">Password</form:label>
                    <form:input type="password" class="input input--rounded" path="password" tabindex="2"/>
                    <c:if test="${not empty error}">
                        <div class="form__alert">
                            <div class="alert alert--danger">
                                <spring:message code="Login.${error}"/>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${not empty expired and expired}">
                        <div class="form__alert">
                            <div class="alert alert--danger">
                                <spring:message code="Session.expired"/>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${not empty logout_sucess}">
                        <div class="form__alert">
                            <div class="alert alert--success">
                                <spring:message code="Logout.${logout_sucess}"/>
                            </div>
                        </div>
                    </c:if>
                </div>
                <div class="form__row form__row--medium form__row--center">
                    <form:button type="submit" class="button button--large" tabindex="3">Login</form:button>
                </div>
            </form:form>
        </section>
        <div class="orichalcos-container">
            <div class="orichalcos-card-container">
                <img src="./images/Orichalcos_Tritos.png" alt="Orichalcos Card Front" class="orichalcos-card-front">
                <img src="./images/yugioh-card-back.jpg" alt="Orichalcos Card Back" class="orichalcos-card-back">
            </div>
            <div class="orichalcos-circle-container">
                <img src="./images/Orichalcos_Tritos_circle.png" alt="Orichalcos Magical Circle" class="orichalcos-circle">
            </div>
        </div>
    </main>
    <jsp:include page="./tiles/footer.jsp" flush="true"/>
</div>
</body>
</html>