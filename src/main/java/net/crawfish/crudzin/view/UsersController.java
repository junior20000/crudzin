/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:59.
 */

package net.crawfish.crudzin.view;

import org.springframework.stereotype.Controller;

/**
 * NOT IMPLEMENTED YET.
 *
 * @author Roberto Schiavelli Júnior
 * @date 13/05/2020
 */
@Controller
public class UsersController {

}
