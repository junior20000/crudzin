/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:59.
 */

package net.crawfish.crudzin.view;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Process exceptions from {@link org.springframework.util.ErrorHandler}.
 *
 * @author Roberto Schiavelli Júnior
 * @date 01/06/2020
 */
@Controller
@RequestMapping("${error.root}")
public class MyErrorController implements ErrorController {

  /**
   *
   */
  private final Environment environment;

  /**
   * Dependency Injection Constructor.
   *
   * @param environment {@link Environment}
   */
  public MyErrorController(Environment environment) {
    this.environment = environment;
  }

  /**
   * Returns the appropriate error view based on it's HTTP response code.
   *
   * @param request request information of HTTP servlets.
   * @param model   {@link ModelMap}
   * @return String : error JSP view {@link org.springframework.web.servlet.View}
   */
  @RequestMapping()
  public String errorResolver(HttpServletRequest request, ModelMap model) {
    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

    if (status != null) {
      int statusCode = Integer.parseInt(status.toString());

      if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
        return "errors/error-500";
      } else if (statusCode == HttpStatus.NOT_FOUND.value()) {
        return "errors/error-404";
      } else if (statusCode == HttpStatus.FORBIDDEN.value()) {
        return "errors/error-403";
      }
    }

    model.addAttribute("errorCode", status);
    return "errors/error";
  }

  /**
   * Get error request path.
   *
   * @return String : error request root
   */
  @Override
  public String getErrorPath() {
    return environment.getProperty("error.root");
  }
}
