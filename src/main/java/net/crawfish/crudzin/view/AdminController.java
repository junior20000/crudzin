/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 17:02.
 */

package net.crawfish.crudzin.view;

import internal.org.springframework.content.rest.controllers.ResourceNotFoundException;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;
import javax.validation.Valid;
import net.crawfish.crudzin.config.AuthenticationFailureCode;
import net.crawfish.crudzin.entities.Role;
import net.crawfish.crudzin.entities.User;
import net.crawfish.crudzin.services.RoleService;
import net.crawfish.crudzin.services.UserService;
import net.crawfish.crudzin.utils.SessionHandler;
import net.crawfish.crudzin.validator.UserEditFormValidator;
import net.crawfish.crudzin.validator.UserValidator;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author Roberto Schiavelli Júnior
 * @date 01/06/2020
 */
@Controller
@RequestMapping("${admin.root}")
public class AdminController {

  private final UserService userService;
  private final RoleService roleService;
  private final UserValidator userValidator;
  private final UserEditFormValidator userEditFormValidator;
  private final Environment environment;
  private final MessageSource messageSource;
  private final SessionHandler sessionHandler;

  /**
   * Argon 2 algorithm password encoder implementation.
   *
   * @see Argon2PasswordEncoder
   * @see org.springframework.security.crypto.password.PasswordEncoder
   */
  private Argon2PasswordEncoder argon2PasswordEncoder =
      new Argon2PasswordEncoder(16, 100, 2, 48, 20);

  /**
   * Dependency Injection Constructor.
   *
   * @param userService           {@link UserService}
   * @param roleService           {@link RoleService}
   * @param userValidator         {@link UserValidator}
   * @param userEditFormValidator {@link UserEditFormValidator}
   * @param environment           Interface responsible for properties implementation
   * @param messageSource         Interface responsible for u18n implementation
   * @param sessionHandler        {@link SessionHandler}
   */
  public AdminController(
      UserService userService,
      RoleService roleService,
      UserValidator userValidator,
      UserEditFormValidator userEditFormValidator,
      Environment environment,
      MessageSource messageSource,
      SessionHandler sessionHandler) {
    this.userService = userService;
    this.roleService = roleService;
    this.userValidator = userValidator;
    this.userEditFormValidator = userEditFormValidator;
    this.environment = environment;
    this.messageSource = messageSource;
    this.sessionHandler = sessionHandler;
  }

  /**
   * Admin login page.
   *
   * @param error    request param with {@link AuthenticationFailureCode#name()} case authentication
   *                 failure
   * @param username request param with username from form case authentication failure
   * @param model    {@link Model}
   * @return String :
   * <ul>
   *   <li>JSP view case not authenticated or authenticated as user.
   *   <li>redirect JSP view {@link #dashboard(Model)} case authenticated as admin.
   * </ul>
   */
  @RequestMapping()
  public String showAdminLoginPage(
      @RequestParam(name = "error", required = false) String error,
      @RequestParam(name = "expired", required = false) boolean expired,
      @RequestParam(name = "username", required = false) String username,
      Model model) {
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    if (!(auth instanceof AnonymousAuthenticationToken)
        && auth.getAuthorities().stream()
        .anyMatch(authority -> authority.getAuthority().equals("ROLE_ADMIN"))) {
      return "forward:" + environment.getProperty("admin.dashboard.urn");
    }

    model.addAttribute("userForm", new User());

    if (error != null) {
      model.addAttribute("error", error);
    }

    if (expired) {
      model.addAttribute("expired", expired);
    }

    if (username != null) {
      model.addAttribute("username", username);
    }

    return "admin_login";
  }

  /**
   * Admin dashboard.
   *
   * @param model {@link Model}
   * @return String : JSP view
   */
  @RequestMapping("${admin.dashboard}")
  public String dashboard(Model model) {
    model.addAttribute("listUsers", userService.listAll());
    return "dashboard";
  }

  /**
   * Add new user form UI.
   *
   * @param model {@link Model}
   * @return String : JSP view
   */
  @RequestMapping("${admin.add.user}")
  public String showAddUserForm(Model model) {
    model.addAttribute("userForm", new User());
    model.addAttribute("role_list", roleService.listAll());
    return "user_add";
  }

  /**
   * Insert new user into DB via POST form action.
   *
   * @param userForm      {@link User} instance from form submit.
   * @param bindingResult {@link BindingResult}
   * @param redir         {@link RedirectAttributes}
   * @param locale        {@link Locale}
   * @param modelMap      {@link ModelMap}
   * @return String :
   * <ul>
   *   <li>JSP view case form error.
   *   <li>redirect JSP view {@link #dashboard(Model)} case success.
   * </ul>
   */
  @PostMapping("${admin.add.user}")
  public String submitAddUser(
      @Valid @ModelAttribute("userForm") User userForm,
      BindingResult bindingResult,
      RedirectAttributes redir,
      Locale locale,
      ModelMap modelMap) {

    modelMap.addAttribute("role_list", roleService.listAll());
    userValidator.validate(userForm, bindingResult);

    if (bindingResult.hasErrors()) {
      return "user_add";
    }

    userForm.setPassword(argon2PasswordEncoder.encode(userForm.getPassword()));
    userService.add(userForm);

    redir.addFlashAttribute(
        "alert_success",
        messageSource.getMessage(
            "admin.add.user.success", new Object[]{userForm.getUsername()}, locale));

    return "redirect:" + environment.getProperty("admin.dashboard.urn");
  }

  /**
   * Edit user form UI via {@link User} id.
   *
   * @param id    path variable {@link User} Long id
   * @param model {@link Model}
   * @return String : JSP view
   */
  @RequestMapping("${admin.edit.user}/{id}")
  public String editUser(@PathVariable("id") Long id, Model model) {
    User user = userService.get(id);

    Set<Role> roleSet = new LinkedHashSet<>();
    roleSet.add(user.getRole());
    roleSet.addAll(roleService.listAll());

    model.addAttribute("user", user);
    model.addAttribute("userForm", new User());
    model.addAttribute("role_list", roleSet);

    return "user_edit";
  }

  /**
   * Update user in DB via POST form submit.
   *
   * @param id            path variable {@link User} Long id
   * @param userForm      {@link User} instance from form submit.
   * @param bindingResult {@link BindingResult}
   * @param locale        {@link Locale}
   * @param modelMap      {@link ModelMap}
   * @param redir         {@link RedirectAttributes}
   * @return String :
   * <ul>
   *   <li>JSP view case form error.
   *   <li>redirect JSP view {@link #dashboard(Model)} case success.
   * </ul>
   */
  @PostMapping("${admin.edit.user}/{id}")
  public String updateUser(
      @PathVariable("id") Long id,
      @Valid @ModelAttribute("userForm") User userForm,
      BindingResult bindingResult,
      Locale locale,
      ModelMap modelMap,
      RedirectAttributes redir) {

    User user = userService.get(id);
    modelMap.addAttribute("user", user);

    Set<Role> roleSet = new LinkedHashSet<>();
    roleSet.add(userForm.getRole());
    roleSet.addAll(roleService.listAll());
    modelMap.addAttribute("role_list", roleSet);

    userEditFormValidator.validate(userForm, bindingResult);

    if (bindingResult.hasErrors()) {
      return "user_edit";
    }

    if (userForm.getUsername() == null || userForm.getUsername().isEmpty()) {
      userForm.setUsername(user.getUsername());
    }

    if (userForm.getPassword() == null || userForm.getPassword().isEmpty()) {
      userForm.setPassword(user.getPassword());
    } else {
      userForm.setPassword(argon2PasswordEncoder.encode(userForm.getPassword()));
    }

    userService.update(id, userForm);

    if (user.getUsername()
        .equals(SecurityContextHolder.getContext().getAuthentication().getName())) {
      sessionHandler.renewPrincipalUserDetails(userForm.getUsername());
    } else if (!user.getRole().getName().equals(userForm.getRole().getName())) {
      sessionHandler.invalidateUserSessions(userForm.getUsername());
    } else {
      sessionHandler.renewUserDetails(user.getUsername(), userForm.getUsername());
    }

    redir.addFlashAttribute(
        "alert_success",
        messageSource.getMessage(
            "admin.edit.user.success", new Object[]{user.getUsername()}, locale));

    return "redirect:" + environment.getProperty("admin.dashboard.urn");
  }

  /**
   * Delete user form UI via {@link User} id.
   *
   * @param id       path variable {@link User} Long id
   * @param modelMap {@link ModelMap}
   * @return String : JSP view
   */
  @GetMapping("${admin.delete.user}/{id}")
  public String confirmDeleteUser(@PathVariable("id") Long id, ModelMap modelMap) {
    final User user = userService.get(id);

    if (user == null) {
      throw new ResourceNotFoundException(); // TODO: Add proper "User not found" page and exception.
    }

    modelMap.addAttribute("user", user);
    return "user_delete";
  }

  /**
   * Delete user in DB via POST form submit.
   *
   * @param id     path variable {@link User} Long id
   * @param locale {@link Locale}
   * @param redir  {@link RedirectAttributes}
   * @return redirect JSP view {@link #dashboard(Model)}
   */
  @PostMapping("${admin.delete.user}/{id}")
  public String deleteUser(@PathVariable("id") Long id, Locale locale, RedirectAttributes redir) {
    final User user = userService.get(id);

    if (user == null) {
      return "redirect:" + environment.getProperty(
          "admin.dashboard.urn"); // TODO: Add proper "User not found" page and exception.
    }

    redir.addFlashAttribute(
        "alert_success",
        messageSource.getMessage(
            "admin.delete.user.success", new Object[]{user.getUsername()}, locale));
    userService.delete(id);
    sessionHandler.invalidateUserSessions(user.getUsername());
    return "redirect:" + environment.getProperty("admin.dashboard.urn");
  }
}
