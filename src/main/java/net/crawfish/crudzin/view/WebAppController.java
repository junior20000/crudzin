/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:05.
 */

package net.crawfish.crudzin.view;

import java.util.Collection;
import net.crawfish.crudzin.entities.User;
import net.crawfish.crudzin.utils.SessionHandler;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * @author Roberto Schiavelli Júnior
 * @date 28/04/2020
 */
@Controller
@RequestMapping()
public class WebAppController {

  private final Environment environment;
  private final SessionHandler sessionHandler;

  /**
   * Dependency Injection Constructor.
   *
   * @param environment    {@link Environment}
   * @param sessionHandler {@link SessionHandler}
   */
  public WebAppController(Environment environment, SessionHandler sessionHandler) {
    this.environment = environment;
    this.sessionHandler = sessionHandler;
  }

  /**
   * Home page (NOT IMPLEMENTED YET).
   *
   * <p>For now: redirects to {@link AdminController#showAdminLoginPage(String, boolean, String,
   * Model)}
   *
   * @return String : redirect JSP view
   */
  @RequestMapping({"", "/home"})
  public String home() {
    return "redirect:/admin";
  }

  /**
   * List all logged in users.
   *
   * @param modelMap {@link ModelMap}
   * @return String : JSP view
   */
  @GetMapping("/online")
  public String onlineUsers(ModelMap modelMap) {
    Collection<User> users = sessionHandler.getAllOnlineUsers();

    if (!users.isEmpty()) {
      modelMap.addAttribute("onlineUsers", users);
    }

    return "online_users";
  }

  /**
   * Logout success logic redirect.
   *
   * @param redir {@link RedirectAttributes}
   * @return String : redirect JSP view
   */
  @GetMapping("/logout_success")
  public String logout_sucess(RedirectAttributes redir) {
    redir.addFlashAttribute("logout_sucess", "Success");
    return "redirect:/admin";
  }

  /**
   * GRUPO HACKEADO KKKKKK!
   *
   * @return String : JSP view
   */
  @GetMapping("/amonimos")
  public String eaeAnonymous() {
    return "easter_egg/anonymous";
  }
}
