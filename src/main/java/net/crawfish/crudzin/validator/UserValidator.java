/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:59.
 */

package net.crawfish.crudzin.validator;

import java.util.regex.Pattern;
import net.crawfish.crudzin.entities.User;
import net.crawfish.crudzin.services.UserService;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Validates login form.
 *
 * @author Roberto Schiavelli Júnior
 * @date 11/05/2020
 * @see net.crawfish.crudzin.view.AdminController#showAdminLoginPage(String, boolean, String, Model)
 */
@Component
public class UserValidator implements Validator {

  private final UserService userService;

  /**
   * Dependency Injection Constructor.
   *
   * @param userService {@link UserService}
   */
  public UserValidator(UserService userService) {
    this.userService = userService;
  }

  /**
   * Assert {@link User} class instance.
   *
   * @param aClass any class
   * @return boolean
   */
  @Override
  public boolean supports(Class<?> aClass) {
    return User.class.equals(aClass);
  }

  /**
   * Performs validation.
   *
   * @param o      object submitted by form
   * @param errors model binder
   */
  @Override
  public void validate(Object o, Errors errors) {
    User user = (User) o;

    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "NotEmpty");
    if ((user.getUsername().length() < 6) || user.getUsername().length() > 32) {
      errors.rejectValue("username", "Size.userForm.username");
    }
    if (Pattern.compile("\\s").matcher(user.getUsername()).find()) {
      errors.rejectValue("username", "WhiteSpace.userForm.username");
    }
    if (userService.findUserByUsername(user.getUsername()) != null) {
      errors.rejectValue("username", "Duplicate.userForm.username");
    }

    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty");
    if ((user.getPassword().length() < 8) || (user.getPassword().length() > 32)) {
      errors.rejectValue("password", "Size.userForm.password");
    }

    ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "NotEmpty");
    if (!user.getConfirmPassword().equals(user.getPassword())) {
      errors.rejectValue("confirmPassword", "Diff.userForm.passwordConfirm");
    }
  }
}
