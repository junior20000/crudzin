/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 15/06/2020 10:29.
 */

package net.crawfish.crudzin;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * Runs application from WAR deployment.
 *
 * @author Roberto Schiavelli Júnior
 * @date 28/04/2020
 */
public class ServletInitializer extends SpringBootServletInitializer {

  /**
   * Loads application configuration.
   *
   * @param application {@link CrudzinApplication}
   * @return {@link SpringApplicationBuilder}
   */
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(CrudzinApplication.class);
  }
}
