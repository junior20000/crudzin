/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:11.
 */

package net.crawfish.crudzin.config;

import net.crawfish.crudzin.services.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

/**
 * Application security configuration.
 *
 * @author Roberto Schiavelli Júnior
 * @date 10/05/2020
 * @see UserDetailsServiceImpl
 */
@Configuration
@Order(1)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  private final UserDetailsServiceImpl userDetailsService;
  private final CustomAuthenticationFailureHandler customAuthenticationFailureHandler;

  /**
   * Dependency Injection Constructor.
   *
   * @param userDetailsService                 {@link UserDetailsServiceImpl}
   * @param customAuthenticationFailureHandler {@link CustomAuthenticationFailureHandler}
   */
  @Autowired
  public SecurityConfiguration(
      UserDetailsServiceImpl userDetailsService,
      CustomAuthenticationFailureHandler customAuthenticationFailureHandler) {
    this.userDetailsService = userDetailsService;
    this.customAuthenticationFailureHandler = customAuthenticationFailureHandler;
  }

  @Bean
  public AuthenticationProvider defaultProvider() {
    DaoAuthenticationProvider impl = new DaoAuthenticationProvider();
    impl.setUserDetailsService(userDetailsService);
    impl.setPasswordEncoder(getPasswordEncoder());
    impl.setHideUserNotFoundExceptions(false);

    return impl;
  }

  @Bean
  public SessionRegistry sessionRegistry() {
    return new SessionRegistryImpl();
  }

  @Bean
  public HttpSessionEventPublisher httpSessionEventPublisher() {
    return new HttpSessionEventPublisher();
  }

  @Bean
  public PasswordEncoder getPasswordEncoder() {
    return new Argon2PasswordEncoder();
  }

  /**
   * Authentication manager setup.
   *
   * @param auth authentication manager bean
   * @throws Exception not implemented yet
   */
  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(defaultProvider());
  }

  /**
   * Web security context setup. Now responsible for ignoring resource content.
   *
   * @param web generic security context
   * @throws Exception not implemented yet
   */
  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring()
        .antMatchers(
            "/resources/**",
            "/static/**",
            "/css/**",
            "/media/**",
            "/images/**",
            "/assets/**",
            "/fonts/**");
  }

  /**
   * Security main thread. Programmatically configuration for specific http request. Includes:
   * request permission, login form configuration, logout configuration and session configuration.
   *
   * @param http request configuration receiver applied to all requests
   * @throws Exception any configuration exception
   */
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
        .antMatchers(
            "/",
            "/admin",
            "/online",
            "/amonimos",
            "/resources/**",
            "/static/**",
            "/css/**",
            "/media/**",
            "/video/**",
            "/images/**",
            "/assets/**",
            "/fonts/**")
        .permitAll()
        .antMatchers("/admin/**")
        .hasRole("ADMIN")
        .anyRequest()
        .authenticated()
        .and()
        // .httpBasic();
        .formLogin()
        .loginPage("/admin")
        .usernameParameter("username")
        .passwordParameter("password")
        .failureHandler(customAuthenticationFailureHandler)
        .defaultSuccessUrl("/admin/dashboard", true)
        .permitAll()
        .and()
        .logout()
        .invalidateHttpSession(true)
        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
        .logoutSuccessUrl("/logout_success")
        .deleteCookies("JSESSIONID")
        .clearAuthentication(true)
        .permitAll();

    http.sessionManagement()
        .invalidSessionUrl("/admin")
        .maximumSessions(1)
        .expiredUrl("/admin?expired=true")
        .sessionRegistry(sessionRegistry());
  }
}
