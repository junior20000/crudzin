/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:59.
 */

package net.crawfish.crudzin.config;

/**
 * Errors that might be raised during authentication.
 *
 * @author Roberto Schiavelli Júnior
 * @date 30/05/2020
 */
public enum AuthenticationFailureCode {
  /**
   * User does not exist
   */
  UsernameNotFound(0),
  /**
   * Password does not match username
   */
  BadCredentials(1);

  /**
   * Hardcoded enum value property
   */
  private final int value;

  AuthenticationFailureCode(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
