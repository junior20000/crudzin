/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:59.
 */

package net.crawfish.crudzin.config;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

/**
 * Handler responsible for any authentication error at login form level.
 *
 * <p>Please check {@link net.crawfish.crudzin.config.AuthenticationFailureCode} to see all
 * exceptions possible at this level.
 *
 * @author Roberto Schiavelli Júnior
 * @date 30/05/2020
 */
@Component
public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

  /**
   * Catch authentication login form exception.
   *
   * @param request  request information of HTTP servlets.
   * @param response response information of HTTP servlets.
   * @param e        {@link AuthenticationException}
   * @throws IOException request / response stream related exception.
   */
  @Override
  public void onAuthenticationFailure(
      HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
      throws IOException {
    if (e.getClass().isAssignableFrom(UsernameNotFoundException.class)) {
      response.sendRedirect(
          request.getRequestURL().toString()
              + "?error="
              + AuthenticationFailureCode.UsernameNotFound.name()
              + "&"
              + "username="
              + request.getParameter("username"));
    } else if (e.getClass().isAssignableFrom(BadCredentialsException.class)) {
      response.sendRedirect(
          request.getRequestURL().toString()
              + "?error="
              + AuthenticationFailureCode.BadCredentials.name()
              + "&"
              + "username="
              + request.getParameter("username"));
    }
  }
}
