/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:59.
 */

package net.crawfish.crudzin.config;

import java.io.File;
import java.io.FileNotFoundException;
import org.springframework.content.commons.repository.Store;
import org.springframework.content.fs.config.EnableFilesystemStores;
import org.springframework.content.fs.io.FileSystemResourceLoader;
import org.springframework.content.rest.StoreRestResource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;

/**
 * Initial setup for real-time file streaming.
 *
 * <p>CLASS NOT FULLY IMPLEMENTED YET, PLEASE IGNORE!
 *
 * @author Roberto Schiavelli Júnior
 * @date 06/06/2020
 */
@Configuration
@EnableFilesystemStores
public class FileStreamConfiguration {

  File filesystemRoot() throws FileNotFoundException {
    return ResourceUtils.getFile("classpath:static/media/video");
  }

  @Bean
  public FileSystemResourceLoader fsResourceLoader() throws Exception {
    return new FileSystemResourceLoader(filesystemRoot().getAbsolutePath());
  }

  @StoreRestResource(path = "videosrc")
  public interface VideoStore extends Store<String> {

  }
}
