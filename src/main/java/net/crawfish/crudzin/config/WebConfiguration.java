/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 15/06/2020 10:29.
 */

package net.crawfish.crudzin.config;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * General application servlet configuration.
 *
 * @author Roberto Schiavelli Júnior
 * @date 12/05/2020
 */
@Configuration
@EnableWebSecurity
@PropertySource("classpath:application.properties")
@PropertySource("classpath:mappings.properties")
public class WebConfiguration implements WebMvcConfigurer {

  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
    return new PropertySourcesPlaceholderConfigurer();
  }

  @Bean
  public MessageSource messageSource() {
    ReloadableResourceBundleMessageSource messageBundle =
        new ReloadableResourceBundleMessageSource();
    messageBundle.setBasename("classpath:messages/messages");
    messageBundle.setDefaultEncoding("UTF-8");
    messageBundle.setFallbackToSystemLocale(true);
    messageBundle.setCacheSeconds(1); // TODO: SET TO CACHE FOREVER AFTER DEV.
    return messageBundle;
  }

  /**
   * Tomcat servlet configurer.
   *
   * @return {@link TomcatServletWebServerFactory}
   */
  @Bean
  public ServletWebServerFactory servletContainer() {
    TomcatServletWebServerFactory tomcat =
        new TomcatServletWebServerFactory() {
          @Override
          protected void postProcessContext(Context context) {
            SecurityConstraint securityConstraint = new SecurityConstraint();
            securityConstraint.setUserConstraint("CONFIDENTIAL");
            SecurityCollection collection = new SecurityCollection();
            collection.addPattern("/*");
            securityConstraint.addCollection(collection);
            context.addConstraint(securityConstraint);
          }
        };
    tomcat.addAdditionalTomcatConnectors(redirectConnector());
    return tomcat;
  }

  /**
   * Connector which redirects any HTTP request to HTTPS.
   *
   * @return {@link Connector}
   */
  private Connector redirectConnector() {
    Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
    connector.setScheme("http");
    connector.setPort(2424);
    connector.setSecure(false);
    connector.setRedirectPort(8443);

    return connector;
  }
}
