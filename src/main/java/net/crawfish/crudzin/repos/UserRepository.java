/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:59.
 */

package net.crawfish.crudzin.repos;

import java.util.Optional;
import net.crawfish.crudzin.entities.Role;
import net.crawfish.crudzin.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Roberto Schiavelli Júnior
 * @date 04/05/2020
 */
public interface UserRepository extends JpaRepository<User, Long> {

  Optional<User> findById(Long id);

  User findByUsername(String username);

  @Transactional
  @Modifying(clearAutomatically = true)
  @Query("UPDATE User u set u.username = ?1, u.password = ?2, u.role = ?3 where u.id = ?4")
  void updateById(String username, String password, Role role, Long ID);
}
