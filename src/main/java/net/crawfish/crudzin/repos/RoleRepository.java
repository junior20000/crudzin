/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:59.
 */

package net.crawfish.crudzin.repos;

import net.crawfish.crudzin.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Roberto Schiavelli Júnior
 * @date 05/05/2020
 */
public interface RoleRepository extends JpaRepository<Role, Long> {

}
