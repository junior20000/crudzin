/*
 * The author disclaims copyright to this source code.  In place of
 * a legal notice, here is a blessing:
 *
 *    May you do good and not evil.
 *    May you find forgiveness for yourself and forgive others.
 *    May you share freely, never taking more than you give.
 *
 */
package net.crawfish.crudzin.dialect;

import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.spi.MetadataBuilderInitializer;
import org.hibernate.dialect.Dialect;
import org.hibernate.engine.jdbc.dialect.internal.DialectResolverSet;
import org.hibernate.engine.jdbc.dialect.spi.DialectResolutionInfo;
import org.hibernate.engine.jdbc.dialect.spi.DialectResolver;
import org.jboss.logging.Logger;

public class SQLiteMetadataBuilderInitializer implements MetadataBuilderInitializer {

  private static final Logger logger = Logger.getLogger(SQLiteMetadataBuilderInitializer.class);

  @Override
  public void contribute(MetadataBuilder metadataBuilder, StandardServiceRegistry serviceRegistry) {
    DialectResolver dialectResolver = serviceRegistry.getService(DialectResolver.class);

    if (!(dialectResolver instanceof DialectResolverSet)) {
      logger.warnf(
          "DialectResolver '%s' is not an instance of DialectResolverSet, not registering SQLiteDialect",
          dialectResolver);
      return;
    }

    ((DialectResolverSet) dialectResolver).addResolver(resolver);
  }

  private static final SQLiteDialect dialect = new SQLiteDialect();

  private static final DialectResolver resolver =
      new DialectResolver() {

        @Override
        public Dialect resolveDialect(DialectResolutionInfo info) {
          if (info.getDatabaseName().equals("SQLite")) {
            return dialect;
          }

          return null;
        }
      };
}
