/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 15/06/2020 10:29.
 */

package net.crawfish.crudzin;

import net.crawfish.crudzin.repos.UserRepository;
import net.crawfish.crudzin.view.AdminController;
import net.crawfish.crudzin.view.WebAppController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author Roberto Schiavelli Júnior
 * @date 28/04/2020
 */
@SpringBootApplication
@ComponentScan(
    basePackageClasses = {WebAppController.class, AdminController.class},
    basePackages = {"net.crawfish.crudzin.*"})
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
public class CrudzinApplication {

  /**
   * Initializes the application.
   *
   * @param args Spring configuration args
   */
  public static void main(String[] args) {
    SpringApplication.run(CrudzinApplication.class, args);
  }
}
