/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:05.
 */

package net.crawfish.crudzin.services;

import java.util.List;
import net.crawfish.crudzin.entities.Role;
import net.crawfish.crudzin.repos.RoleRepository;
import org.springframework.stereotype.Service;

/**
 * @author Roberto Schiavelli Júnior
 * @date 05/05/2020
 */
@Service
public class RoleService {

  private final RoleRepository repo;

  /**
   * Dependency Injection Constructor.
   *
   * @param repo {@link RoleRepository}
   */
  public RoleService(RoleRepository repo) {
    this.repo = repo;
  }

  public List<Role> listAll() {
    return repo.findAll();
  }

  public void add(Role role) {
    repo.save(role);
  }

  public Role get(Long id) {
    return repo.findById(id).get();
  }

  public void delete(Long id) {
    repo.deleteById(id);
  }
}
