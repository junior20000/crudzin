/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:59.
 */

package net.crawfish.crudzin.services;

import java.util.List;
import net.crawfish.crudzin.entities.User;
import net.crawfish.crudzin.repos.UserRepository;
import org.springframework.stereotype.Service;

/**
 * @author Roberto Schiavelli Júnior
 * @date 04/05/2020
 */
@Service
public class UserService {

  private final UserRepository repo;

  /**
   * Dependency Injection Constructor.
   *
   * @param repo {@link UserRepository}
   */
  public UserService(UserRepository repo) {
    this.repo = repo;
  }

  public List<User> listAll() {
    return repo.findAll();
  }

  public User findUserByUsername(String username) {
    return repo.findByUsername(username);
  }

  public void update(Long id, User user) {
    repo.updateById(user.getUsername(), user.getPassword(), user.getRole(), id);
  }

  public void add(User user) {
    repo.save(user);
  }

  public User get(Long id) {
    return repo.findById(id).isPresent() ? repo.findById(id).get() : null;
  }

  public void delete(Long id) {
    repo.deleteById(id);
  }
}
