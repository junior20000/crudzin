/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:19.
 */

package net.crawfish.crudzin.utils;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Roberto Schiavelli Júnior
 * @date 12/05/2020
 */
public class JwtUtil {

  public static String generateToken(String signingKey, String subject) {
    Date now = new Date(System.currentTimeMillis());

    JwtBuilder jwtBuilder =
        Jwts.builder()
            .setSubject(subject)
            .setIssuedAt(now)
            .signWith(SignatureAlgorithm.ES256, signingKey);

    return jwtBuilder.compact();
  }

  public static String getSubject(
      HttpServletRequest httpServletRequest, String jwtTokenCookieName, String signinKey) {
    String token = CookieUtil.getValue(httpServletRequest, jwtTokenCookieName);

    if (token == null) {
      return null;
    }

    return Jwts.parser().setSigningKey(signinKey).parseClaimsJws(token).getBody().getSubject();
  }
}
