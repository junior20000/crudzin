/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:59.
 */

package net.crawfish.crudzin.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.crawfish.crudzin.entities.User;
import net.crawfish.crudzin.services.MyUserDetails;
import net.crawfish.crudzin.services.UserDetailsServiceImpl;
import net.crawfish.crudzin.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

/**
 * Responsible for controlling principals.
 *
 * @author Roberto Schiavelli Júnior
 * @date 08/06/2020
 */
@Component
public class SessionHandler {

  private final UserService userService;
  private final UserDetailsServiceImpl userDetailsService;
  private final SessionRegistry sessionRegistry;

  /**
   * Dependency Injection Constructor.
   *
   * @param userService        {@link UserService}
   * @param userDetailsService {@link UserDetailsServiceImpl}
   * @param sessionRegistry    session registry
   */
  public SessionHandler(
      UserService userService,
      UserDetailsServiceImpl userDetailsService,
      SessionRegistry sessionRegistry) {
    this.userService = userService;
    this.userDetailsService = userDetailsService;
    this.sessionRegistry = sessionRegistry;
  }

  /**
   * Checks if a principal has any active sessions.
   *
   * @param principal {@link MyUserDetails}
   * @return <code>true</code> if there are active user sessions; <code>false</code> otherwise.
   */
  private boolean hasActiveSessions(MyUserDetails principal) {
    Collection<SessionInformation> activeSessions =
        sessionRegistry.getAllSessions(principal, false);
    return !activeSessions.isEmpty();
  }

  /**
   * Find user <code>MyUserDetails</code> via username.
   *
   * @param username user username to be found.
   * @return <code>MyUserDetails</code> if any session found; <code>null</code> otherwise.
   * @see MyUserDetails
   * @see SessionRegistry
   */
  private MyUserDetails findAuthenticatedUserDetails(String username) {
    List<Object> principals = sessionRegistry.getAllPrincipals();
    for (final Object principal : principals) {
      final MyUserDetails userDetails = (MyUserDetails) principal;
      if (username.equals(userDetails.getUsername())) {
        return userDetails;
      }
    }
    return null;
  }

  /**
   * Get all active sessions from a user.
   *
   * @param userDetails {@link MyUserDetails}
   * @return active session information collection
   * @see SessionInformation
   */
  private Collection<SessionInformation> getActiveSessions(MyUserDetails userDetails) {
    Collection<SessionInformation> activeSessions =
        sessionRegistry.getAllSessions(userDetails, false);
    if (!activeSessions.isEmpty()) {
      return activeSessions;
    }

    return null;
  }

  /**
   * Expires all {@link SessionInformation} within collection.
   *
   * @param sessions {@link SessionInformation} collection
   */
  private void expireAllSessions(Collection<SessionInformation> sessions) {
    for (final SessionInformation session : sessions) {
      session.expireNow();
    }
  }

  /**
   * Expires and removes a specific session from {@link SessionRegistry}.
   *
   * <p>{@link User} don't need to re-authenticate.
   *
   * @param session {@link SessionInformation}
   */
  private void expireAndRemoveSession(SessionInformation session) {
    session.expireNow();
    sessionRegistry.removeSessionInformation(session.getSessionId());
  }

  /**
   * Expires and removes all sessions within a collection from {@link SessionRegistry}.
   *
   * <p>{@link User} don't need to re-authenticate.
   *
   * @param sessions {@link SessionInformation} collection
   * @see #expireAndRemoveSession(SessionInformation)
   */
  private void expireAndRemoveAllSessions(Collection<SessionInformation> sessions) {
    for (final SessionInformation session : sessions) {
      expireAndRemoveSession(session);
    }
  }

  /**
   * Same as {@link #getAllOnlineUsers()}. However, this method retrieves one specific user, based
   * on it's username.
   *
   * @param username user username
   * @return {@link User}
   * @threadsafety true
   */
  public User getOnlineUser(String username) {
    final List<Object> principals = sessionRegistry.getAllPrincipals();

    if (principals != null) {
      for (Object principal : principals) {
        if (username.equals(principal)) {
          if (!sessionRegistry.getAllSessions(principal, false).isEmpty()) {
            return userService.findUserByUsername(username);
          }
        }
      }
    }

    return new User();
  }

  /**
   * Gets all logged in principals and filters the expired / invalidated sessions.
   *
   * @return logged in user {@link User}
   * @threadsafety true
   */
  public Collection<User> getAllOnlineUsers() {
    List<Object> principals = sessionRegistry.getAllPrincipals();
    Collection<User> users = new ArrayList<>();
    if (!principals.isEmpty()) {
      for (final Object principal : principals) {
        final MyUserDetails userDetails = (MyUserDetails) principal;
        if (hasActiveSessions(userDetails)) {
          users.add(userService.findUserByUsername(userDetails.getUsername()));
        }
      }
    }

    return users;
  }

  /**
   * Invalidates all sessions of a {@link User} via username.
   *
   * @param username user username
   */
  public void invalidateUserSessions(String username) {
    final MyUserDetails userDetails = findAuthenticatedUserDetails(username);

    if (userDetails == null) {
      // User is not logged in
      return;
    }

    Collection<SessionInformation> sessions = getActiveSessions(userDetails);

    if (sessions != null) {
      expireAllSessions(sessions);
    }
  }

  /**
   * Reloads principal based on the context username.
   *
   * @see Authentication
   * @see MyUserDetails
   */
  public void reloadPrincipalUserDetails() {
    Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    MyUserDetails userDetails =
        (MyUserDetails)
            userDetailsService.loadUserByUsername(((MyUserDetails) principal).getUsername());
    Authentication authentication =
        new PreAuthenticatedAuthenticationToken(
            userDetails, userDetails.getPassword(), userDetails.getAuthorities());

    SecurityContextHolder.getContext().setAuthentication(authentication);
  }

  /**
   * Reload principal based on username {@link UserDetailsServiceImpl#loadUserByUsername(String)}
   * call.
   *
   * @param username {@link MyUserDetails} username
   * @see Authentication
   */
  public void reloadPrincipalUserDetails(String username) {
    MyUserDetails userDetails = (MyUserDetails) userDetailsService.loadUserByUsername(username);
    Authentication authentication =
        new PreAuthenticatedAuthenticationToken(
            userDetails, userDetails.getPassword(), userDetails.getAuthorities());

    SecurityContextHolder.getContext().setAuthentication(authentication);
  }

  /**
   * Renew a specific user session and authentication after a DB update query.
   *
   * <p>DOES NOT WORK WITH AUTHORITY RELOAD!
   *
   * @param oldUsername user username before update
   * @param newUsername user username after update
   * @see #findAuthenticatedUserDetails(String)
   * @see Authentication
   * @see SessionInformation
   */
  public void renewUserDetails(String oldUsername, String newUsername) {
    MyUserDetails oldUserDetails = findAuthenticatedUserDetails(oldUsername);

    if (oldUserDetails == null) {
      // User is not logged in
      return;
    }

    Collection<SessionInformation> sessions = getActiveSessions(oldUserDetails);

    if (sessions != null) {
      MyUserDetails newUserDetails =
          (MyUserDetails) userDetailsService.loadUserByUsername(newUsername);

      for (SessionInformation session : sessions) {
        expireAndRemoveSession(session);
        sessionRegistry.registerNewSession(session.getSessionId(), newUserDetails);
      }
    }
  }

  /**
   * Renew principal session and authentication.
   *
   * <p>May be renamed to <code>migratePrincipalUserDetails</code>.
   *
   * @param newUsername new {@link MyUserDetails} username that should replace the principal session
   *                    and authentication
   * @see Authentication
   * @see SessionInformation
   */
  public void renewPrincipalUserDetails(String newUsername) {
    MyUserDetails oldUserDetails =
        (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    Collection<SessionInformation> sessions = getActiveSessions(oldUserDetails);

    reloadPrincipalUserDetails(newUsername);
    Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    if (sessions != null) {
      for (SessionInformation session : sessions) {
        expireAndRemoveSession(session);
        sessionRegistry.registerNewSession(session.getSessionId(), principal);
      }
    }
  }
}
