/*
 * Copyright 2020, Roberto Schiavelli Júnior - All Rights Reserved
 * Unauthorized copying of this file (content included) or any other within this project, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Roberto Schiavelli Júnior
 * Last time modified: 16/06/2020 16:19.
 */

package net.crawfish.crudzin.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.util.WebUtils;

/**
 * @author Roberto Schiavelli Júnior
 * @date 12/05/2020
 */
public class CookieUtil {

  public static void create(
      HttpServletResponse httpServletResponse,
      String name,
      String value,
      Boolean secure,
      Integer maxAge,
      String domain) {
    Cookie cookie = new Cookie(name, value);
    cookie.setSecure(secure);
    cookie.setHttpOnly(true);
    cookie.setMaxAge(maxAge);
    cookie.setDomain(domain);
    cookie.setPath("/");
    httpServletResponse.addCookie(cookie);
  }

  public static void clear(HttpServletResponse httpServletResponse, String name) {
    Cookie cookie = new Cookie(name, null);
    cookie.setPath("/");
    cookie.setHttpOnly(true);
    cookie.setMaxAge(0);
    httpServletResponse.addCookie(cookie);
  }

  public static String getValue(HttpServletRequest httpServletRequest, String name) {
    Cookie cookie = WebUtils.getCookie(httpServletRequest, name);
    return cookie != null ? cookie.getValue() : null;
  }
}
