# CRUDzin

> Projeto Integrado Uninove 5º Semestre

Crudzin is a simple web-app which lets you perform simple [CRUD](https://en.wikipedia.org/wiki/Create,_read,_update_and_delete) statements.

## Project Structure

#### Class Diagram
![alt text--Class-Diagram](readme/CRUDzin CD.png "Class Diagram")

#### Maven Dependencies
![alt text--Maven-Dep](readme/CRUDzin Maven Dep.png "Maven Dependencies")

## Getting Started
This instructions should be more than enough to get you a copy of the software up-and-running for testing and development purposes. For deployment, please check the [Deployment](#deployment) section.

### Prerequisites
You may need a few requisites before starting:
 * [Java 8](https://www.java.com/download/) or superior
 * [Tomcat](https://tomcat.apache.org/download-90.cgi) 8 or superior
 * [Maven](https://maven.apache.org/download.cgi) `OPTIONAL`
> Maven is fully optional and your IDE may already have built-in support for it.

You may find your java version with this simple command:
````shell script
java -version
````

If you have Maven installed, you can use this command too:
````shell script
mvn -version
````

### Initialization
You can boot the project using this command in conjunction with Maven:
`````shell script
spring-boot:run
`````

### Application config.
The application configuration can be found under:
 * `application.properties`
 * `WebConfiguration.java`

Inside `resources` and `<main_package>\config`, respectively.

#### Starter User Info.
Any info. regarding the start users in the project can be found at the `users.txt` file.

## Running the tests
There is no tests to be ran ATM (*tests not implemented yet*).

## Deployment
No deployment instructions available for this project ATM.

## Built With
 * [Maven](https://maven.apache.org/download.cgi) - Dependency manager
 * [Spring](https://spring.io/) - Web framework
 * [Tomcat](http://tomcat.apache.org/) - Java web server

## Contributing
The project is **NOT** open to contributions.

## Code Style
 * Java: [Google Java Style](https://google.github.io/styleguide/javaguide.html)
 * CSS: [Concentric CSS](https://github.com/brandon-rhodes/Concentric-CSS/blob/master/style3.css)
 
 > You can find more info about Concentric CSS [here](https://rhodesmill.org/brandon/2011/concentric-css/).

## Versioning
I use [GIT](https://git-scm.com/) for versioning.

## Author
 * [Roberto Schiavelli Júnior](https://www.linkedin.com/in/roberto-schiavelli-j%C3%BAnior-86a3561a9/) - June 2020

## Disclaimer
The ***Lon3 W0lf Corporation*** is just a joke (Lon3 W0lf is my project group name and the Corporation is a reference to the *Kaiba Corporation*).  
Consequently, I have no connections with that company if the same exists.

## Copyright
© Roberto Schiavelli Júnior - All Rights Reserved  
Unauthorized copying of this project and any files within it, via any medium is strictly prohibited  
Proprietary and confidential